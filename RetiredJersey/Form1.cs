﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RetiredJersey
{
    public partial class Form1 : Form
    {
        /* Dictionary<int, JerseyNumber> retiredNumber = new Dictionary<int, JerseyNumber>()
          {
              {3, new JerseyNumber("Babe Ruth", 1948) },
              {4, new JerseyNumber("Lou Gehrig", 1948) },
              {5, new JerseyNumber("Joe Dimaggio", 1952) },
              {7, new JerseyNumber("Mickey Mantle", 1969) },
              {8, new JerseyNumber("Yoge Berra", 1972) },
              {10, new JerseyNumber("Phil Rizzuto", 1985) },
              {23, new JerseyNumber("Don Mattingly", 1997) },
              {42, new JerseyNumber("Jackie Robinson", 1993) },
              {44, new JerseyNumber("Reggie Jackson", 1993) },
          };*/
        
          Dictionary<string, Dictionary<int, JerseyNumber>> teamNameDictionary
              = new Dictionary<string, Dictionary<int, JerseyNumber>>() {
                  {"New York Yankees", new Dictionary<int, JerseyNumber>(){
                          {3, new JerseyNumber("Babe Ruth", 1948) },
                          {4, new JerseyNumber("Lou Gehrig", 1948) },
                          {5, new JerseyNumber("Joe Dimaggio", 1952) },
                          {7, new JerseyNumber("Mickey Mantle", 1969) },
                          {8, new JerseyNumber("Yoge Berra", 1972) },
                          {10, new JerseyNumber("Phil Rizzuto", 1985) },
                          {23, new JerseyNumber("Don Mattingly", 1997) },
                          {42, new JerseyNumber("Jackie Robinson", 1993) },
                          {44, new JerseyNumber("Reggie Jackson", 1993) },
                      }
                  },
                  {"Baltimore Orioles", new Dictionary<int, JerseyNumber>(){
                          {8, new JerseyNumber("Cal Ripken,Jr.", 1972) },
                      }
                  },
              };
        Dictionary<int, JerseyNumber> retireNumbers;
        public Form1()
        {
            InitializeComponent();
            foreach (string team in teamNameDictionary.Keys)
            teamComboBox.Items.Add(team);
        }
        private void teamComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            redraw();
            retireNumbers = teamNameDictionary[(string)teamComboBox.SelectedItem];
            //retireNumbers = teamNameDictionary[(TeamName)teamComboBox.SelectedItem];
            foreach (int jerseyNumberKey in retireNumbers.Keys)
                jerseyNumberComboBox.Items.Add(jerseyNumberKey);
        }
        private void jerseyNumberComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            JerseyNumber jerseyNumber = retireNumbers[(int)jerseyNumberComboBox.SelectedItem];
            playerTextBox.Text = jerseyNumber.Player;
            YearTextBox.Text = jerseyNumber.YearRetired.ToString();
        }
        private void redraw()
        {
            //檢查combobox選擇的項目和dictionary.key是否相同
            //否則將combobox清空
            if (teamComboBox.SelectedItem != teamNameDictionary.Keys)
                jerseyNumberComboBox.Items.Clear();
        }
    }
}